using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ViewAmiibos : MonoBehaviour
{
	public GameObject boardSquare;
	public int totalColumns = 10;

	void Start()
	{
		if (FindObjectOfType<LoadAmiibos>() == null)  //if this object does not exist, add and load the data
		{
			this.gameObject.AddComponent<LoadAmiibos>().loadXML();
		}

		buildGrid();
	}

	public void buildGrid()
	{

		Amiibo[] currentLayout = FindObjectOfType<LoadAmiibos>().amiibos.ToArray();

		if (SceneManager.GetActiveScene().name == "AmiiboPairs")
		{
			currentLayout = getRandomGridLayout(6, 6); //36 - 6 columns and 6 rows
			totalColumns = 6;
		}

		int rows = 0;
		int column = 0;

		for (int i = 0; i < currentLayout.Length; i++)
		{
			GameObject square = Instantiate(boardSquare) as GameObject;
			square.AddComponent<Tile>();
			square.GetComponent<Tile>().amiibo = currentLayout[i];
			square.transform.position = new Vector3(((-totalColumns / 2.0f) + column + .5f) * (square.GetComponent<Renderer>().bounds.size.x + .11f),
													 3.5f - ((square.GetComponent<Renderer>().bounds.size.y + .2f) * rows),
													 0);
			square.GetComponent<Collider>().enabled = true;

			column++;
			if (column == totalColumns)
			{
				column = 0;
				rows++;
			}
		}
	}

	private Amiibo[] getRandomGridLayout(int rows, int columns)
	{
		int tilesNeeded = rows * columns;
		Amiibo[] layout = new Amiibo[tilesNeeded];

		//this for loop populates the Array in Pairs
		for (int i = 0; i < tilesNeeded; i += 2)  //count in 2 as working in pairs
		{
			Amiibo pickAmiibo = FindObjectOfType<LoadAmiibos>().amiibos[Random.Range(0, FindObjectOfType<LoadAmiibos>().amiibos.Count)];
			layout[i] = pickAmiibo;
			layout[i + 1] = pickAmiibo;
		}

		layout = Shuffle(layout); //this will randomise the layout - comment the line out to see the unrandomised layout


		return layout;
	}

	private Amiibo[] Shuffle(Amiibo[] layout)
	{
		// Loops through array
		for (int i = layout.Length - 1; i > 0; i--)
		{
			// Randomize a number between 0 and i (so that the range decreases each time)
			int rnd = Random.Range(0, i);

			// Save the value of the current i, otherwise it'll overwrite when we swap the values
			Amiibo temp = layout[i];

			// Swap the new and old values
			layout[i] = layout[rnd];
			layout[rnd] = temp;
		}

		return layout;

	}

	public void buttonEventToBuildNewGrid()
	{
		Tile[] tiles = FindObjectsOfType<Tile>();
		foreach(Tile tile in tiles)
		{
			Destroy(tile.gameObject);
		}
		Invoke("buildGrid", 0.35f);
	}
}